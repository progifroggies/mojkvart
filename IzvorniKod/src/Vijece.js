var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {
    $scope.vijteme = []
    $scope.editV = false;
    $scope.deleteV = false;
    $scope.showTemaButton = false;
    $scope.showTemaGoButton = false;
    var vm = this;
    vm.showTemaText = [];
    vm.vijNewTemaDesc = [];
    vm.deleteTemaChoice = [];

    $scope.vjecnik = false;

    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });

    }

    $scope.role = 0

    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
            "userId": localStorage.getItem("userId")
        }
    ).then(function(data) {
        //console.log(data)
        $scope.role = data.data.role;
        //console.log($scope.role)
        if (data.data.userId == null) {
            $window.location.href = "Login.html";
        }
        if ($scope.role == 3) {
            $window.location.href = "admin.html";
        } else if ($scope.role == -1) {
            $window.location.href = "Login.html";
        }
        if (data.data.blocked) {
            $window.location.href = "Login.html";
        }
        if ($scope.role == 2) {
            $scope.vjecnik = true;
        }
    });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }

    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/hall", {
            'userId': localStorage.getItem("userId")
        }
    ).then(function(data) {
        //console.log(JSON.stringify(data.data))
        // if userid = userid od teme pokazi edit button
        // $scope.editComm
        // $scope.deleteComm
        var i = 0
        data.data[1].forEach(element => {
            $scope.vijteme[i] = element
            vm.showTemaText[i] = false;
            vm.vijNewTemaDesc[i] = "";
            vm.deleteTemaChoice.push(false);
            i++;
        });
        data.data[0].forEach(element => {
            $scope.vijteme[i] = element
            i++;
        });
    })

    vm.edit = function(ind) {

    }

    vm.deleteTemaShow = function(ind) {
        vm.deleteTemaChoice[ind] = true;
        allPosts = $scope.vijteme.length
    }

    vm.deleteTemaNo = function(ind) {
        vm.deleteTemaChoice[ind] = false;
        allPosts = $scope.vijteme.length;
    }

    vm.delete = function(ind) {
        if ($scope.role == 1) $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/hall/deleteReport", {
                'idIzvjesca': $scope.vijteme[ind].idIzvjesca,
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            window.location.reload();
        })
        else $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/hall/deleteOwnReport", {
                'idIzvjesca': $scope.vijteme[ind].idIzvjesca,
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            window.location.reload();
        })
    }

    vm.temaBtn = function(ind) {
        //console.log("a")
        if ($scope.vijteme[ind].sadrzaj != null) {
            if ($scope.vijteme[ind].tema == null) { //textbox add
                vm.showTemaText[ind] = true;
            } else {
                window.location.href = "Forum.html#!/" + $scope.vijteme[ind].tema.idTeme.toString();
            }
        }
    }

    vm.temaBtnPost = function(ind) {
        //console.log(vm.vijNewTemaDesc[ind])
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/add", {
                'idIzvjesca': $scope.vijteme[ind].idIzvjesca,
                "userId": localStorage.getItem("userId"),
                'naslov': $scope.vijteme[ind].naslov,
                'sadrzaj': vm.vijNewTemaDesc[ind]
            }
        ).then(function(data) {
            //console.log(data)
            window.location.href = "Forum.html#!/" + data.data.idTeme.toString();
        })
    }

    $scope.getVijName = function(ind) {
        return $scope.vijteme[ind].naslov;
    }
    $scope.getPerson = function(ind) {
        return "Objavio/la " + $scope.vijteme[ind].user.name + " " + $scope.vijteme[ind].user.surname;
    }
    $scope.getVijDesc = function(ind) {
        //console.log($scope.vijteme[ind].userId);
        //console.log($scope.vijteme[ind])
        if ($scope.vijteme[ind].userId == localStorage.getItem("userId") || $scope.role == 1) {
            vm.deleteV = true;
            vm.editV = true;
        } else {
            vm.deleteV = false;
            vm.editV = false;
        }
        if ($scope.vijteme[ind].sadrzaj != null) {
            if ($scope.vijteme[ind].tema != null) {
                vm.showTemaGoButton = true;
                vm.showTemaButton = false;
            } else {
                vm.showTemaGoButton = false;
                vm.showTemaButton = true;
            }
        } else {
            vm.showTemaGoButton = false;
            vm.showTemaButton = false;
        }
        return $scope.vijteme[ind].sadrzaj;
    }
    var firstTema = 0;
    var firstVij = 0;
    var allPosts = 0;
    $scope.getVijType = function(ind) {
        if (allPosts == ($scope.vijteme.length)) {
            allPosts = 0;
            firstVij = 0;
            firstTema = 0;
        }
        if (($scope.vijteme[ind].sadrzaj == null) && (firstTema == 0)) {
            firstTema = 1;
            allPosts++;
            return "Buduće teme:";

        } else if (($scope.vijteme[ind].sadrzaj != null) && (firstVij == 0)) {
            firstVij = 1;
            allPosts++;
            return "Izvješća:";
        } else {
            allPosts++;
            return null;
        }

    }

    var DatumNew = null;
    $scope.getVijDate = function(ind) {
        if ($scope.vijteme[ind].month < 10) {
            mjesec = "0" + $scope.vijteme[ind].month.toString();
        } else {
            mjesec = $scope.vijteme[ind].month.toString();
        }
        if ($scope.vijteme[ind].day < 10) {
            dan = "0" + $scope.vijteme[ind].day.toString();
        } else {
            dan = $scope.vijteme[ind].day.toString();
        }
        DatumNew = dan + "/" + mjesec + "/" + $scope.vijteme[ind].year.toString();
        return DatumNew; //date

    }


    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }

    vm.NewVijece = false;
    vm.NewBuducaTema = false;
    var vm = this;
    vm.NewBudT = function() {
        vm.NewVijece = false;
        if (
            vm.NewBuducaTema == true) {
            vm.NewBuducaTema = false;
        } else {
            vm.NewBuducaTema = true;
        }

    }
    vm.NewVij = function() {
        vm.NewBuducaTema = false;
        if (
            vm.NewVijece == true) {
            vm.NewVijece = false;
        } else {
            vm.NewVijece = true;
        }
    }

    let mybutton = document.getElementById("btn-back-to-top");
    let buttonplus = document.getElementById("btn-open-form");
    let buttonplus2 = document.getElementById("btn-open-tema");
    let addpostbutton = document.getElementById("AddPost");

    window.onscroll = function() {
        scrollFunction();
    };

    /** function openform() {
        if (
            $scope.NewVijece == true) {
            $scope.NewVijece = false;
        } else {
            $scope.NewVijece = true;
        }
    };

    function openform2() {
        if (
            $scope.NewBuducaTema == true) {
            $scope.NewBuducaTema = false;
        } else {
            $scope.NewBuducaTema = true;
        }
    };

    function AddNewPost() {

        //add post to datapase and post it on the page
        newform.style.display = "none";
    }
 */
    function scrollFunction() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    mybutton.addEventListener("click", backToTop);
    //buttonplus.addEventListener("click", openform);
    //buttonplus2.addEventListener("click", openform2);
    //addpostbutton.addEventListener("click", AddNewPost);

    function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    vm.showWrongDateIz = false;
    vm.showWrongDateBud = false;

    vm.addNewVij = function() {
        var mjesec;
        var dan;
        if ((vm.vijDatum.getMonth() + 1) < 10) {
            mjesec = "0" + (vm.vijDatum.getMonth() + 1).toString();
        } else {
            mjesec = (vm.vijDatum.getMonth() + 1).toString();
        }
        if (vm.vijDatum.getDate() < 10) {
            dan = "0" + vm.vijDatum.getDate().toString();
        } else {
            dan = vm.vijDatum.getDate().toString();
        }
        $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/hall/add", {
                    'userId': localStorage.getItem("userId"),
                    'naslov': vm.vijNaslov,
                    'sadrzaj': vm.vijOpis,
                    'datumIzvjesca': (vm.vijDatum.getYear() + 1900).toString() + "-" +
                        mjesec + "-" + dan

                })
            .then(function(data) {
                //console.log("OVDJE")
                //console.log(data)
                vm.showWrongDateIz = false;
                vm.showWrongNameIz = false;
                vm.showBudExistsIz = false;
                vm.showIzExistsIz = false;
                if (data.data.idIzvjesca == null && data.data.idIzvjesca == null) { //krivi datum
                    vm.showWrongDateIz = true;
                }
                if (data.data.datumIzvjesca == null && data.data.idIzvjesca != null) { //naslov postoji
                    vm.showWrongNameIz = true;
                    if (data.data.sadrzaj == null) {
                        vm.showBudExistsIz = true;
                    } else vm.showIzExistsIz = true;
                } else if (data.data.datumIzvjesca != null) window.location.reload();
            })
    }

    vm.addNewVijTema = function() {
        //console.log((vm.vijDatumTema.getYear() + 1900).toString() + "-" + (vm.vijDatumTema.getMonth() + 1).toString() + "-" + vm.vijDatumTema.getDate().toString());
        var mjesec;
        var dan;
        if ((vm.vijDatumTema.getMonth() + 1) < 10) {
            mjesec = "0" + (vm.vijDatumTema.getMonth() + 1).toString();
        } else {
            mjesec = (vm.vijDatumTema.getMonth() + 1).toString();
        }
        if (vm.vijDatumTema.getDate() < 10) {
            dan = "0" + vm.vijDatumTema.getDate().toString();
        } else {
            dan = vm.vijDatumTema.getDate().toString();
        }
        $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/hall/topic", {
                    'userId': localStorage.getItem("userId"),
                    'naslov': vm.vijNaslovTema,
                    'datumIzvjesca': (vm.vijDatumTema.getYear() + 1900).toString() + "-" +
                        mjesec + "-" + dan
                })
            .then(function(data) {
                //console.log(data)
                //console.log(data.data.year)
                vm.showWrongDateBud = false;
                vm.showWrongNameBud = false;
                vm.showBudExistsBud = false;
                vm.showIzExistsBud = false;
                if (data.data.idIzvjesca == null && data.data.idIzvjesca == null) { //krivi datum
                    vm.showWrongDateBud = true;
                }
                if (data.data.datumIzvjesca == null && data.data.idIzvjesca != null) { //naslov postoji
                    vm.showWrongNameBud = true;
                    if (data.data.sadrzaj == null) {
                        vm.showBudExistsBud = true;
                    } else vm.showIzExistsBud = true;
                } else if (data.data.datumIzvjesca != null) window.location.reload();
            })
    }




});