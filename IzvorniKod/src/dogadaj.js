var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {

    $scope.dogtemenew = [];
    $scope.dogteme = [];
    $scope.CloseEditDogShow = false;
    $scope.EditDogShow = true;

    var vm = this;

    vm.deleteDogChoice = []
    vm.deleteDogBefChoice = []

    vm.RemoveDogAfter = false;
    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });


    }

    $scope.role = 0

    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
            "userId": localStorage.getItem("userId")
        }
    ).then(function(data) {
        //console.log("KAJ")
        //console.log(data)
        $scope.role = data.data.role;
        //console.log($scope.role)
        if (data.data.userId == null) {
            $window.location.href = "Login.html";
        }
        if ($scope.role == 1) {
            $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/events/suggestions", {
                    "userId": localStorage.getItem("userId")
                }
            ).then(function(data) {
                $scope.dogtemenew = data.data;
            });


        }
        if ($scope.role != 1) {
            $scope.ModOnly = false;
            $scope.ModOnly2 = false;
        } else {
            $scope.ModOnly = true;
            $scope.ModOnly2 = true;
        }
        if ($scope.role == 3) {
            $window.location.href = "admin.html";
        } else if ($scope.role == -1) {
            $window.location.href = "Login.html";
        }
        if (data.data.blocked) {
            $window.location.href = "Login.html";
        }
    });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }



    load = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events", {
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            $scope.dogteme = data.data;
            //console.log($scope.dogtemenew);
            //console.log(data.data);
            for (const dog in data.data) {
                vm.deleteDogChoice.push(false)
                vm.deleteDogBefChoice.push(false)
            }
        });
    }
    load();
    $scope.pastDate = false;
    vm.addNewDog = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/add", {
                "naziv": vm.naziv,
                "userId": localStorage.getItem("userId"),
                "mjesto": vm.mjesto,
                "year": vm.datum.getYear() + 1900,
                "month": vm.datum.getMonth() + 1,
                "day": vm.datum.getDate(),
                "hour": vm.vrijeme.getHours(),
                "minute": vm.vrijeme.getMinutes(),
                "trajanje": vm.trajanje,
                "kratkiOpis": vm.opis,
                "vidljivost": false
            }
        ).then(function(data) {
            //console.log(data.data.idDogadjaja);
            if (data.data.idDogadjaja == null) {
                $scope.pastDate = true;
            } else {
                load();
                window.location.reload();
            }
        });

    }

    $scope.getDogName = function(ind) {
        if ($scope.dogteme[ind].userId == localStorage.getItem("userId") || $scope.role == 1) {
            vm.RemoveDogAfter = true;
        } else {
            vm.RemoveDogAfter = false;
        }
        return $scope.dogteme[ind].naziv;
    }

    $scope.getDogMjesto = function(ind) {
        return $scope.dogteme[ind].mjesto;
    }

    $scope.getDogOpis = function(ind) {
        return $scope.dogteme[ind].kratkiOpis;
    }

    $scope.getDogTrajanje = function(ind) {
        return $scope.dogteme[ind].trajanje + " min";
    }

    var finalDatum = null;
    $scope.getDogDatum = function(ind) {
        finalDatum = ($scope.dogteme[ind].day).toString() + "/" + $scope.dogteme[ind].month.toString() + "/" + $scope.dogteme[ind].year.toString();
        return finalDatum; //date
    }
    var finalTime = null;
    $scope.getDogVrijeme = function(ind) {
        if ($scope.dogteme[ind].minute < 10) {
            finalTime = $scope.dogteme[ind].hour.toString() + ":0" + $scope.dogteme[ind].minute.toString();
        } else {
            finalTime = $scope.dogteme[ind].hour.toString() + ":" + $scope.dogteme[ind].minute.toString();
        }
        return finalTime; //time
    }

    //Moderator

    $scope.getDogNameNew = function(ind) {
        vm.noMoreDogNew = false;
        return $scope.dogtemenew[ind].naziv;
    }

    $scope.getDogMjestoNew = function(ind) {
        return $scope.dogtemenew[ind].mjesto;
    }

    $scope.getDogOpisNew = function(ind) {
        return $scope.dogtemenew[ind].kratkiOpis;
    }

    $scope.getDogTrajanjeNew = function(ind) {
        return $scope.dogtemenew[ind].trajanje + " min";
    }

    $scope.getIdNew = function(ind) {
        return $scope.dogtemenew[ind].idDogadjaja;

    }

    if ($scope.dogtemenew == 0) {
        vm.noMoreDogNew = true;
    } else {
        vm.noMoreDogNew = false;
    }

    var finalDatumNew = null;
    $scope.getDogDatumNew = function(ind) {
        finalDatumNew = ($scope.dogtemenew[ind].day).toString() + "/" + $scope.dogtemenew[ind].month.toString() + "/" + $scope.dogtemenew[ind].year.toString();
        return finalDatumNew; //date
    }
    var finalTimeNew = null;
    $scope.getDogVrijemeNew = function(ind) {
        if ($scope.dogtemenew[ind].minute < 10) {
            finalTimeNew = $scope.dogtemenew[ind].hour.toString() + ":0" + $scope.dogtemenew[ind].minute.toString();
        } else {
            finalTimeNew = $scope.dogtemenew[ind].hour.toString() + ":" + $scope.dogtemenew[ind].minute.toString();
        }
        return finalTimeNew; //time
    }

    $scope.getPerson = function(ind) {
        return "Objavio/la " + $scope.dogteme[ind].user.name + " " + $scope.dogteme[ind].user.surname;

    }
    $scope.getPersonNew = function(ind) {
            return "Objavio/la " + $scope.dogtemenew[ind].user.name + " " + $scope.dogtemenew[ind].user.surname;
        }
        //Dolasci
    $scope.getDolazak = function(ind) {
        if ($scope.dogteme[ind].userReaction == 0) {
            return $scope.dogteme[ind].coming + " 🠔";
        }
        return $scope.dogteme[ind].coming;
    }
    $scope.getMDolazak = function(ind) {
        if ($scope.dogteme[ind].userReaction == 2) {
            return $scope.dogteme[ind].maybeComing + " 🠔";
        }
        return $scope.dogteme[ind].maybeComing;
    }

    $scope.getNeDolazak = function(ind) {
        if ($scope.dogteme[ind].userReaction == 1) {
            return $scope.dogteme[ind].notComing + " 🠔";
        }
        return $scope.dogteme[ind].notComing;
    }

    vm.Dolazim = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/rsvp", {
                "idDogadjaja": $scope.dogteme[ind].idDogadjaja,
                "userId": localStorage.getItem("userId"),
                "status": 0
            }
        ).then(function(data) {

            window.location.reload();
        });
    }
    vm.MDolazim = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/rsvp", {
                "idDogadjaja": $scope.dogteme[ind].idDogadjaja,
                "userId": localStorage.getItem("userId"),
                "status": 2
            }
        ).then(function(data) {
            window.location.reload();
        });

    }
    vm.NeDolazim = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/rsvp", {
                "idDogadjaja": $scope.dogteme[ind].idDogadjaja,
                "userId": localStorage.getItem("userId"),
                "status": 1
            }
        ).then(function(data) {
            window.location.reload();
        });
    }



    vm.approveDog = function(ind) {
        if ($scope.EditDogShow == true) {
            //console.log($scope.dogtemenew[ind].idDogadjaja);
            $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/events/suggestions/accept", {
                    "idDogadjaja": $scope.dogtemenew[ind].idDogadjaja,
                    "naziv": $scope.dogtemenew[ind].naziv,
                    "mjesto": $scope.dogtemenew[ind].mjesto,
                    "year": $scope.dogtemenew[ind].year,
                    "month": $scope.dogtemenew[ind].month,
                    "day": $scope.dogtemenew[ind].day,
                    "hour": $scope.dogtemenew[ind].hour,
                    "minute": $scope.dogtemenew[ind].minute,
                    "trajanje": $scope.dogtemenew[ind].trajanje,
                    "kratkiOpis": $scope.dogtemenew[ind].kratkiOpis
                }
            ).then(function(data) {
                window.location.reload();
            });
        } else {
            //console.log("Prvo treba zatvoriti uređivanje!");
        }

    }

    vm.RemoveDog = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/suggestions/delete", {
                "idDogadjaja": $scope.dogtemenew[ind].idDogadjaja
            }
        ).then(function(data) {
            window.location.reload();
        });
    }

    vm.deleteDogShow = function(ind) {
        vm.deleteDogChoice[ind] = true;
    }

    vm.deleteDogNo = function(ind) {
        vm.deleteDogChoice[ind] = false;
    }

    vm.deleteDogBefShow = function(ind) {
        vm.deleteDogBefChoice[ind] = true;
    }

    vm.deleteDogBefNo = function(ind) {
        vm.deleteDogBefChoice[ind] = false;
    }

    vm.RemoveDogAft = function(ind) {
        //console.log("a")
        //console.log($scope.dogteme[ind].user.userId)
        if ($scope.dogteme[ind].user.userId == localStorage.getItem('userId')) $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/deleteOwnEvent", {
                "userid": localStorage.getItem('userId'),
                "idDogadjaja": $scope.dogteme[ind].idDogadjaja
            }
        ).then(function(data) {
            window.location.reload();
        });
        else $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/deleteEvent", {
                "userid": localStorage.getItem('userId'),
                "idDogadjaja": $scope.dogteme[ind].idDogadjaja
            }
        ).then(function(data) {
            window.location.reload();
        });


    }
    $scope.OdobriDog = true;
    $scope.RemoveDogShow = true;
    $scope.EditDog = true;
    $scope.enableEdit = false;
    $scope.dontEdit = true;
    vm.EditDog = function(ind) {
        //vm.nonEditBtn.contentsEditable = false;

        $scope.EditDogShow = false;
        $scope.CloseEditDogShow = true;
        $scope.OdobriDog = false;
        $scope.RemoveDogShow = false;
        $scope.enableEdit = true;
        $scope.CloseEditStop = true;
        vm.idEdit = $scope.dogtemenew[ind].idDogadjaja;
        vm.nazivEdit = $scope.dogtemenew[ind].naziv;
        vm.mjestoEdit = $scope.dogtemenew[ind].mjesto;
        vm.opisEdit = $scope.dogtemenew[ind].kratkiOpis;
    }
    vm.EditDogClose = function(ind) {

        $scope.CloseEditDogShow = false;
        $scope.CloseEditStop = false;
        $scope.EditDogShow = true;
        $scope.OdobriDog = true;
        $scope.RemoveDogShow = true;
        $scope.enableEdit = false;
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/events/suggestions/accept", {
                "idDogadjaja": vm.idEdit,
                "naziv": vm.nazivEdit,
                "mjesto": vm.mjestoEdit,
                "kratkiOpis": vm.opisEdit
            }
        ).then(function(data) {
            window.location.reload();
            load();
        });
    }
    vm.EditStop = function() {
        $scope.CloseEditDogShow = false;
        $scope.CloseEditStop = false;
        $scope.EditDogShow = true;
        $scope.OdobriDog = true;
        $scope.RemoveDogShow = true;
        $scope.enableEdit = false;
    }


    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }


    let mybutton = document.getElementById("btn-back-to-top");
    let buttonplus = document.getElementById("btn-open-form");
    let newform = document.getElementById("newform");
    let addpostbutton = document.getElementById("AddPost");
    window.onscroll = function() {
        scrollFunction();
    };
    //console.log(buttonplus.style.display);

    function openform() {
        if (
            newform.style.display == "block") {
            newform.style.display = "none";
        } else {
            newform.style.display = "block";
        }
    };

    //function AddNewPost() {

    //add post to datapase and post it on the page
    //  newform.style.display = "none";
    //}

    function scrollFunction() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    mybutton.addEventListener("click", backToTop);
    buttonplus.addEventListener("click", openform);
    //addpostbutton.addEventListener("click", AddNewPost);

    function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }






});