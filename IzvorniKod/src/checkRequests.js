var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {

    //$scope.zahtjevi = JSON.parse(localStorage.getItem('zahtjevi'))
    //$scope.users = JSON.parse(localStorage.getItem('users'))

    $scope.zahtjevi = []
    $scope.users = []
    $scope.showlist = []
    $scope.listempty = 1

    $scope.role = 0
    
    $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            //console.log(data)
            $scope.role = data.data.role;
            //console.log($scope.role)
            if ($scope.role != 3) {
                $window.location.href = "Forum.html";
            }
        });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }

    $http.get(
        "https://progifroggies-moj-kvart-be.herokuapp.com/checkRequests"
    ).then(function(data) {
        //console.log(JSON.stringify(data.data))
        $scope.zahtjevi = data.data
        for (const zahtjev in $scope.zahtjevi) {
            $scope.showlist.push(0)
            $scope.listempty = 0
        }
            $http.get(
                "https://progifroggies-moj-kvart-be.herokuapp.com/users"
            ).then(function(data) {
                $scope.users = data.data
                //console.log(JSON.stringify(data.data))
            });
        //console.log(JSON.stringify($scope.ulice))
    });
    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });
    }

    $scope.setShow = function(ind, val) {
        $scope.showlist[ind] = val
    }

    $scope.acceptRequest = function(ind) {
        //console.log($scope.zahtjevi[ind].idZahtjeva + " " + ind.toString())
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/checkRequests/accept", {
                'userId': localStorage.getItem("userId"),
                'idZahtjeva': $scope.zahtjevi[ind].idZahtjeva
            }
        ).then(function(data) {
            window.location.reload()
        });
    }

    $scope.denyRequest = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/checkRequests/deny", {
                'idZahtjeva': $scope.zahtjevi[ind].idZahtjeva
            }
        ).then(function(data) {
            window.location.reload()
        });
    }

    $scope.getRoleStringAcc = function(ind) {
        var rolestrs = ['stanovnika', 'moderatora', 'vijećnika', 'administratora']
        return rolestrs[ind]
    }

    $scope.getZahtjevString = function(ind) {
        //console.log($scope.zahtjevi[ind])
        tuser = 0
        for (const user in $scope.users) {
            if ($scope.users[user].userId == $scope.zahtjevi[ind].userId) tuser = user;
        }
        return "Korisnik " + $scope.users[tuser].name + " " + $scope.users[tuser].surname + " (" + $scope.users[tuser].email + ") traži promjenu uloge u " + $scope.getRoleStringAcc($scope.zahtjevi[ind].vrstaZatrazenogRolea);
    }

    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    updatedark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains("darkmode") != checkdark()) elements[i].classList.toggle("darkmode");
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }




});