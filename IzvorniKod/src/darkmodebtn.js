var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {
	checkdark = function() {
		var wastrue = 0;
		let decodedCookie = decodeURIComponent(document.cookie);
		//alert(decodedCookie);
  		let ca = decodedCookie.split(';');
  		for(let i = 0; i <ca.length; i++) {
		    let c = ca[i];
		    while (c.charAt(0) == ' ') {
		      c = c.substring(1);
		    }
		    if (c.indexOf("darkmode") == 0) {
		    	//alert("found");
		    	//alert(c);
		    	if (c == 'darkmode=TRUE') {
		    		wastrue = 1;
		    	}
		    }
		}
		return wastrue;
	}
	swapdark = function() {
		//alert("test")
		elements = document.querySelectorAll("*")
		for (i = 0; i < elements.length; i++) {
			elements[i].classList.toggle("darkmode")
    	}
    	element = document.getElementById("btn-darkmode")
		if (element.classList.contains("darkmode")) {
			element.innerHTML = "<b>DARK MODE</b>";
		} else {
			element.innerHTML = "<b>LIGHT MODE</b>";
		}
	}
	$scope.darkmodefunc = function() {
		swapdark();
		var wastrue = checkdark();
		var cookie;
		var d = new Date();
  		d.setTime(d.getTime() + (365*24*60*60*1000));
  		let expires = "expires="+ d.toUTCString();
		if (wastrue) {
			cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
		} else {
			cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
		}
		//alert(cookie);
		document.cookie = cookie;
	}
	
	if (checkdark()) {
		swapdark();
	}
});