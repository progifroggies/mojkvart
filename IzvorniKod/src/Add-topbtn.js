let mybutton = document.getElementById("btn-back-to-top");
let buttonplus = document.getElementById("btn-open-form");
let newform = document.getElementById("newform");
let addpostbutton = document.getElementById("AddPost");
window.onscroll = function() {
    scrollFunction();
};

function openform() {
    newform.style.display = "block";
    buttonplus.style.display = "none";

};

function AddNewPost() {

    //add post to datapase and post it on the page
    newform.style.display = "none";
    buttonplus.style.display = "block";
}

function scrollFunction() {
    if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
    ) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}
mybutton.addEventListener("click", backToTop);
buttonplus.addEventListener("click", openform);
addpostbutton.addEventListener("click", AddNewPost);

function backToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}