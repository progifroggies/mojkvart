var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {
    $scope.userrole = null;
    $scope.NewAddress = false;
    var vm = this;
    $scope.exists = false;
    $scope.rolear = [true, true, true, true];
    $scope.changeto = [false, false, false, false];
    $scope.rolenames = ['Korisnik', 'Moderator', 'Vijećnik', 'Administrator'];
    $scope.ForumAdm = true;
    $scope.DogAdm = true;
    $scope.VijAdm = true;
    $scope.AdmAct = false;

    $scope.adminNotShow1 = true;
    $scope.adminNotShow2 = true;
    $scope.getRoleShow = function(ind) {
        return ind.toString() + " - " + $scope.rolenames[ind];
    }
    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
            'userId': localStorage.getItem("userId")
        }
    ).then(function(data) {
        // alert(data.data.email);
        $scope.userrole = data.data.role;
        if (data.data.email != null) {
            //console.log(data.data);
            $scope.email = data.data.email;
            $scope.name = data.data.name;
            $scope.surname = data.data.surname;
            $scope.streetId = data.data.streetName;
        }
        //console.log(data.data.zahtjev);
        if (data.data.zahtjev != null) {
            $scope.exists = true;
            $scope.changeto[data.data.zahtjev.vrstaZatrazenogRolea] = true;
            //console.log($scope.changeto)
        }
        if ($scope.userrole != null) {
            $scope.rolear[$scope.userrole] = false;
            for (let i = 0; i < 4; i++) {
                if ($scope.changeto[i]) $scope.rolear[i] = false;
            }
            //console.log($scope.rolear)
            if ($scope.userrole == 0) {
                $scope.stanovnik = true;
                $scope.moderator = false;
                $scope.vjecnik = false;
                $scope.administrator = false;
                $scope.AdmAct = false;
            }
            if ($scope.userrole == 1) {
                $scope.stanovnik = false;
                $scope.moderator = true;
                $scope.vjecnik = false;
                $scope.administrator = false;
                $scope.AdmAct = false;
            }
            if ($scope.userrole == 2) {
                $scope.stanovnik = false;
                $scope.moderator = false;
                $scope.vjecnik = true;
                $scope.administrator = false;
                $scope.AdmAct = false;
            }
            if ($scope.userrole == 3) {
                $scope.stanovnik = false;
                $scope.moderator = false;
                $scope.vjecnik = false;
                $scope.administrator = true;
                $scope.streetId = "Administratori nemaju adresu";
                $scope.adminNotShow1 = false;
                $scope.adminNotShow2 = false;
                $scope.ForumAdm = false;
                $scope.DogAdm = false;
                $scope.VijAdm = false;
                $scope.AdmAct = true;
            }

        } else {
            window.location.href = "Login.html"
        }

        return;
        $window.location.reload();

    });

    $scope.ChangeAddress = function() {
        if ($scope.NewAddress == false) {
            $scope.NewAddress = true;
            document.getElementById("ChangeAddress").text = "Odustani";
            document.getElementById("ChangeAddress").style.background = "#8b0000";
            document.getElementById("ChangeRole").style.display = "none";
        } else {
            document.getElementById("ChangeAddress").text = "Promjeni adresu";
            document.getElementById("ChangeAddress").style.background = "#ff6600";
            document.getElementById("ChangeRole").style.display = "";
            $scope.NewAddress = false;
        }

    }

    $scope.ChangeRole = function() {
        if ($scope.NewRole != true) {
            $scope.NewRole = true;
            document.getElementById("ChangeRole").text = "Odustani";
            document.getElementById("ChangeRole").style.background = "#8b0000";
            document.getElementById("ChangeAddress").style.display = "none";
        } else {
            document.getElementById("ChangeRole").text = "Promjeni ulogu";
            document.getElementById("ChangeRole").style.background = "#ff6600";
            document.getElementById("ChangeAddress").style.display = "";
            $scope.NewRole = false;
        }

    }

    $scope.showNoUlica = false;

    $scope.sendaddress = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/changeaddress", {
                'userId': localStorage.getItem("userId"),
                'streetName': $scope.address
            }
        ).then(function(data) {
            if (data.data.email != null) {
                localStorage.setItem("userId", data.data.userId);
                $scope.email = data.data.email;
                $scope.name = data.data.name;
                $scope.surname = data.data.surname;
                $scope.streetId = data.data.streetName;
                $scope.NewAddress = false;
                $window.location.reload();
                // return;
            } else {
                $scope.showNoUlica = true;
            }
        });

    }

    var roleswap;
    $scope.sendrole = function() {
        if (document.getElementById('age1').checked) {
            roleswap = document.getElementById('age1').value;
        }
        if (document.getElementById('age2').checked) {
            roleswap = document.getElementById('age2').value;
        }
        if (document.getElementById('age3').checked) {
            roleswap = document.getElementById('age3').value;
        }
        if (document.getElementById('age4').checked) {
            roleswap = document.getElementById('age4').value;
        }
        //console.log(roleswap);
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/request/add", {
                'userId': localStorage.getItem("userId"),
                'vrstaZatrazenogRolea': roleswap
            }
        ).then(function(data) {
            // return;
            $scope.NewRole = false;
            $window.location.reload();
        });

    }

    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });


    }

    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }




    // $http.post(
    //     "https://progifroggies-moj-kvart-be.herokuapp.com/changeaddress", {}
    // ).then(function(data) {
    //     // alert(data.data.email);
    //     if (data.data.email != null) {
    //         $scope.email = data.data.email;
    //         $scope.name = data.data.name;
    //         $scope.surname = data.data.surname;
    //         $scope.streetId = data.data.streetId;
    //         return;
    //     }
    //     $window.location.reload();

    // });
});