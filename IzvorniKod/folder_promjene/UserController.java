package MojKvart.Projekt.rest;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.AssertTrue;

import MojKvart.Projekt.domain.*;
import MojKvart.Projekt.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import MojKvart.Projekt.dao.TemaRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("")
public class UserController {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	@Autowired
	private UlicaService ulicaService;

	@Autowired
	private TemaService temaService;

  @Autowired
  private IzvjesceService izvjesceService;

	@Autowired
	private CetvrtService cetvrtService;

	@Autowired
	private IzbrisanService izbrisanService;

	@Autowired
	private ZahtjevUlogeService zahtjevUlogeService;

	@Autowired
	private OdgovorService odgovorService;

	@Autowired
	private DogadjanjeService dogadjanjeService;

	@PostMapping("/register")
	public ResponseEntity<User> registerUser(@RequestBody UserDTO user, HttpServletRequest request, HttpServletResponse response) {
//		System.out.println(request.getCookies());
//		System.out.println(response);
//		if (request.getCookies() == null)
//			response.addCookie(new Cookie("email", "e"));
//		System.out.println(response.getHeader("Set-Cookie"));

		Optional<User> optionalUser = userService.findByEmail(user.getEmail());
        if(optionalUser.isPresent()) {
        	System.out.println("Postoji");
        	User newUser = optionalUser.get();
        	newUser.setEmail(null);
            return new ResponseEntity<>(newUser, HttpStatus.OK);
        } else {
        	Optional<Izbrisan> izbrisan = izbrisanService.findByEmail(user.getEmail());

        	if (izbrisan.isPresent())
        		// TODO: to znaci da je korisnik trajno obrisan
        		// TODO: jos uvijek ne radi jer tablica nije napravljena
        		return new ResponseEntity<>(new User(), HttpStatus.OK);

            System.out.println("Ne postoji");
            User returnUser = userService.createUser(userService.changeIntoUser(user));
            System.out.println(returnUser);
            if (returnUser == null)
            	return new ResponseEntity<>(new User(), HttpStatus.OK);
            return new ResponseEntity<>(returnUser, HttpStatus.CREATED);
        }
    }

	@PostMapping("/login")
	public ResponseEntity<User> loginUser(@RequestBody UserDTO user) {
        if(userService.loginUser(user.getEmail(), user.getPasswordNoHash()).isPresent()) {
        	System.out.println("Tocno");
        	Optional<User> returnUser = userService.findByEmail(user.getEmail());
        	return new ResponseEntity<>(returnUser.get(), HttpStatus.OK);
        } else {
        	System.out.println("Netocno");
        	Optional<User> returnUser = userService.findByEmail(user.getEmail());
        	if (returnUser.isPresent()) { // TODO: s ovim se vidi da email postoji ali da je sifra kriva
        		User returnUserNonOptional = returnUser.get();
        		returnUserNonOptional.setEmail(null);
        		return new ResponseEntity<>(returnUserNonOptional, HttpStatus.OK);
        	}

            return new ResponseEntity<>(new User(), HttpStatus.OK);
        }
    }

	@PostMapping("/logoutUser")
	public void logout() {
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//
//		Assert.isTrue(auth.isAuthenticated(), "User is not logged in!");
		System.out.println("Logout");
//		auth.setAuthenticated(false);
	}

	@PostMapping("/profil")
	public ResponseEntity<UserDTO> showUser(@RequestBody User user) {
		Optional<User> returnUser = userService.findByUserId(user.getUserId());

		if (!returnUser.isPresent())
			return new ResponseEntity<>(new UserDTO(), HttpStatus.OK);

		UserDTO returnUserDTO = userService.changeIntoUserDTO(returnUser.get());


		return new ResponseEntity<>(returnUserDTO, HttpStatus.OK);
	}

	@PostMapping("/changeaddress")
	public ResponseEntity<User> changeUserAddress(@RequestBody UserDTO user) {
		User returnUser = userService.changeUserAddress(user);

		System.out.println(returnUser);

		if (returnUser == null)
			return new ResponseEntity<>(new User(), HttpStatus.OK);

		return new ResponseEntity<>(returnUser, HttpStatus.OK);
	}

	// funkcije za administratore

	@GetMapping("/neighborhoods")
	public ResponseEntity<List<Cetvrt>> getAllCetvrti() {
		return new ResponseEntity<>(cetvrtService.getAllCetvrti(), HttpStatus.OK);
	}

	@PostMapping("/neighborhoods/add")
	public ResponseEntity<Cetvrt> addNewCetvrt(@RequestBody Cetvrt cetvrt) {
		Optional<Cetvrt> existing = cetvrtService.findByImeCetvrti(cetvrt.getImeCetvrti());
		if (existing.isPresent())
			return new ResponseEntity<>(new Cetvrt(), HttpStatus.OK);

		Cetvrt returnCetvrt = cetvrtService.createCetvrt(cetvrt);
		return new ResponseEntity<>(returnCetvrt, HttpStatus.OK);

	}

	@GetMapping("/neighborhoods/{id}")
	public List<Ulica> getAllUlicaByIdCetvrti(@PathVariable(value="id") long idCetvrti) {

//		return ulicaService.getAllByIdCetvrti(Integer.toUnsignedLong(idCetvrti));
		return ulicaService.getAllByIdCetvrti(idCetvrti);
	}

	@PostMapping("/neighborhoods/{id}/add")
	public Ulica addUlica(@PathVariable(value="id") long idCetvrti, @RequestBody Ulica ulica) {
		Optional<Ulica> existing = ulicaService.findByImeUlice(ulica.getImeUlice());
		if (existing.isPresent())
			return new Ulica();

		Optional<Cetvrt> existingCetvrt = cetvrtService.findByIdCetvrti(idCetvrti);
		if (!existingCetvrt.isPresent())
			return null;

		return ulicaService.createUlica(new Ulica(ulica.getImeUlice(), idCetvrti));
	}

	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@PostMapping("/users/changeRole")
	public User changeUserRole(@RequestBody User user) {
		return userService.changeRole(user.getUserId(), user.getRole());
	}

	@PostMapping("/users/ban")
	public User banUser(@RequestBody User user) {
		// TODO: U Spring Security se moze activateat i deactivateat user
		return userService.banUser(user.getUserId());
	}

	@PostMapping("/users/unban")
	public User unbanUser(@RequestBody User user) {
		// TODO: U Spring Security se moze activateat i deactivateat user
		return userService.unbanUser(user.getUserId());
	}

	@PostMapping("/users/delete")
	public void deleteUser(@RequestBody User user) {
		userService.deleteUser(user.getUserId());
	}

	@GetMapping("/napunibazu")
	public void napuniBazu() {
		cetvrtService.createCetvrt(new Cetvrt("Krk"));
		cetvrtService.createCetvrt(new Cetvrt("Zagreb"));

		ulicaService.createUlica(new Ulica("Street", 1L));
		ulicaService.createUlica(new Ulica("StreetZ", 2L));
		ulicaService.createUlica(new Ulica("Streetyyy", 1L));

		userService.createUser(new User("Katarina", "katarina.brnic@fer.hr", "Brnić", passwordEncoder.encode("pass1"), 3L));
		userService.createUser(new User("Miroslav", "miroslav.heroku@fer.hr", "Heroku", passwordEncoder.encode("pass2"), 4L));
		userService.createUser(new User("Edi", "edi.kolac@fer.hr", "Kolač", passwordEncoder.encode("pass3"), 5L, 2, false));

		userService.createUser(new User("Admin", "admin.adminic@fer.hr", "Adminić", passwordEncoder.encode("passAdmin"), null, 3, false));

		temaService.createTema(new Tema("Uciti kako uciti", 6L, null));
		temaService.createTema(new Tema("Osobni i socijalni razvoj", 6L, null));
		temaService.createTema(new Tema("Građanski odgoj i obrazovanje", 7L, null));
		// TODO: bit ce izvjesce kasnije
		temaService.createTema(new Tema("Inicijativa čišćenja grada", 8L, null));

		zahtjevUlogeService.createZahtjevUloge(new ZahtjevUloge(2, 6L));
		zahtjevUlogeService.createZahtjevUloge(new ZahtjevUloge(3, 7L));

		odgovorService.createOdgovor(new Odgovor(6L, 10L, "Za više smjernice se javite na moj Instagram account, gdje svaki dan objavljujem nove savjete o učenju."));
		odgovorService.createOdgovor(new Odgovor(8L, 10L, "I mene stvarno zanima kako bih mogao lakše učiti tokom ovih vremena."));
		odgovorService.createOdgovor(new Odgovor(7L, 12L, "Kako Zagreb postupa po pitanju građanskog odgoja i obrazovanja?"));
		odgovorService.createOdgovor(new Odgovor(8L, 13L, "Očistio sam svojoj djevojci sobu, mogu li otvoriti firmu?"));
		odgovorService.createOdgovor(new Odgovor(6L, 11L, "Kako?"));

		dogadjanjeService.createDogadjanje(new Dogadjanje("Overwatch Tournament", "Your PC", new Date(121, 11, 16), 180, "Igramo Overwatch!", true, 8L));
		dogadjanjeService.createDogadjanje(new Dogadjanje("LoL Tournament", "Igralište", new Date(122, 0, 16), 180, "Igramo!", true, 6L));
		dogadjanjeService.createDogadjanje(new Dogadjanje("Dota Tournament", "Park", new Date(122, 01, 15), 120, "Igramo jako puno!", true, 6L));
		dogadjanjeService.createDogadjanje(new Dogadjanje("Food Party", "Na stolu", new Date(122, 0, 18), 60, "Jedemo!", true, 7L));






  }

}
