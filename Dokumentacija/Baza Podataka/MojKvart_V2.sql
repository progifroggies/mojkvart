CREATE TABLE CETVRT
(
  idCetvrti INT NOT NULL,
  imeCetvrti VARCHAR(30) NOT NULL,
  PRIMARY KEY (idCetvrti)
);

CREATE TABLE ULICA
(
  idUlice INT NOT NULL,
  imeUlice VARCHAR(100) NOT NULL,
  idCetvrti INT NOT NULL,
  PRIMARY KEY (idUlice),
  FOREIGN KEY (idCetvrti) REFERENCES CETVRT(idCetvrti)
);

CREATE TABLE IZBRISAN
(
  email VARCHAR(100) NOT NULL,
  PRIMARY KEY (email)
);

CREATE TABLE KORISNIK
(
  KorisnikID INT NOT NULL,
  sifra INT NOT NULL,
  ime VARCHAR(50) NOT NULL,
  prezime VARCHAR(50) NOT NULL,
  emailAdresa VARCHAR(100) NOT NULL,
  uloga INT NOT NULL,
  blokiran INT NOT NULL,
  idUlice INT NOT NULL,
  PRIMARY KEY (KorisnikID),
  CONSTRAINT blokiranCheck CHECK (blokiran = 1 OR blokiran = 0),
  FOREIGN KEY (idUlice) REFERENCES ULICA(idUlice),
  UNIQUE (emailAdresa)
);

CREATE TABLE DOGADANJE
(
  idDogadanja INT NOT NULL,
  naziv VARCHAR(50) NOT NULL,
  mjesto VARCHAR(100) NOT NULL,
  vrijeme DATE NOT NULL,
  trajanje INT NOT NULL,
  kratkiOpis VARCHAR(1000) NOT NULL,
  vidljivost INT NOT NULL,
  KorisnikID INT NOT NULL,
  idCetvrti INT NOT NULL,
  PRIMARY KEY (idDogadanja),
  CONSTRAINT vidljivostCheck CHECK (vidljivost = 1 OR vidljivost = 0),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID),
  FOREIGN KEY (idCetvrti) REFERENCES CETVRT(idCetvrti)
);

CREATE TABLE ZAHTJEVULOGE
(
  idZahtjeva INT NOT NULL,
  vrstaZatrazenogRolea INT NOT NULL,
  KorisnikID INT NOT NULL,
  CONSTRAINT roleReqCheck CHECK (vrstaZatrazenogRolea = 1 OR vrstaZatrazenogRolea = 0),
  PRIMARY KEY (idZahtjeva, KorisnikID),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID)
);

CREATE TABLE KorisnikRSVP
(
  KorisnikID INT NOT NULL,
  idDogadanja INT NOT NULL,
  PRIMARY KEY (KorisnikID, idDogadanja),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID),
  FOREIGN KEY (idDogadanja) REFERENCES DOGADANJE(idDogadanja)
);

CREATE TABLE IZVJESCE
(
  IDIzvjesca INT NOT NULL,
  naslov VARCHAR(50) NOT NULL,
  datumIzvjesca DATE NOT NULL,
  sadrzaj VARCHAR(10000) NOT NULL,
  KorisnikID INT NOT NULL,
  PRIMARY KEY (IDIzvjesca),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID)
);

CREATE TABLE TEMA
(
  IDTeme INT NOT NULL,
  naslov VARCHAR(40) NOT NULL,
  datumTeme DATE NOT NULL,
  brojOdgovora INT DEFAULT 1 NOT NULL,
  IDIzvjesca INT,
  KorisnikID INT NOT NULL,
  PRIMARY KEY (IDTeme),
  FOREIGN KEY (IDIzvjesca) REFERENCES IZVJESCE(IDIzvjesca),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID)
);

CREATE TABLE ODGOVOR
(
  Sadrzaj VARCHAR(5000) NOT NULL,
  datumOdgovora DATE NOT NULL,
  brojLajkova INT DEFAULT 0 NOT NULL,
  idOdgovora INT NOT NULL,
  IDTeme INT NOT NULL,
  KorisnikID INT NOT NULL,
  PRIMARY KEY (idOdgovora, IDTeme),
  FOREIGN KEY (IDTeme) REFERENCES TEMA(IDTeme),
  FOREIGN KEY (KorisnikID) REFERENCES KORISNIK(KorisnikID)
);