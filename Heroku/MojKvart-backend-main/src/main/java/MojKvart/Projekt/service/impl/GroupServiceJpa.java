package MojKvart.Projekt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import MojKvart.Projekt.dao.*;
import MojKvart.Projekt.domain.Group;
import MojKvart.Projekt.domain.Student;
import MojKvart.Projekt.service.GroupService;
import MojKvart.Projekt.service.RequestDeniedException;
import MojKvart.Projekt.service.StudentService;

@Service
public class GroupServiceJpa implements GroupService {

	@Autowired
	private GroupRepository groupRepo;
	
	@Autowired
	private StudentService studentService;
	
	@Override
	public List<Group> listAll() {
		return groupRepo.findAll();
	}
	
	@Override
	public Group createGroup(String groupName, String coordinatorJmbag) {
		Student coordinator = studentService.findByJmbag(coordinatorJmbag);
		if (coordinator == null) {
			throw new RequestDeniedException("No student with JMBAG " + coordinatorJmbag);
		}
		Group g = groupRepo.findByMember(coordinator);
		if (g != null) {
			throw new RequestDeniedException(coordinator + " already a member of " + g);
		}
		return groupRepo.save(new Group(groupName, coordinator));
	}
	
}
