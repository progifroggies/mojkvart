package MojKvart.Projekt.service.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import MojKvart.Projekt.dao.StudentRepository;
import MojKvart.Projekt.domain.Student;
import MojKvart.Projekt.service.RequestDeniedException;
import MojKvart.Projekt.service.StudentService;

@Service
public class StudentServiceJpa implements StudentService {

	@Autowired
	private StudentRepository studentRepo;
	
	@Override
	public List<Student> listAll() {
		return studentRepo.findAll();
	}
	
	private static final String JMBAG_FORMAT = "[0-9]{10}";

	@Override
	public Student createStudent(Student student) {
		Assert.notNull(student, "Student object must be given");
//		Assert.isNull(student.getId(),
//				"Student ID must be null, not " + student.getId());
		String jmbag = student.getJmbag();
		Assert.hasText(jmbag, "JMBAG must be given!");
		Assert.isTrue(jmbag.matches(JMBAG_FORMAT),
				"JMBAG must have 10 digits, not '" + jmbag + "'");
		if (studentRepo.countByJmbag(student.getJmbag()) > 0)
			throw new RequestDeniedException(
					"Student with JMBAG " + student.getJmbag() + " already exists"
			);
		return studentRepo.save(student);
	}
	
	public Student findByJmbag(String jmbag) {
		Assert.notNull(jmbag, "JMBAG must be given!");
		return studentRepo.findByJmbag(jmbag);
	}
	
}
