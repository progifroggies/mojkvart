package MojKvart.Projekt.service;

import java.util.*;

import MojKvart.Projekt.domain.Student;

public interface StudentService {

	List<Student> listAll();
	
	Student createStudent(Student student);
	
	Student findByJmbag(String jmbag);
	
}
