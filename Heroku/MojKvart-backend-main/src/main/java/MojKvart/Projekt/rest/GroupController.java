package MojKvart.Projekt.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import MojKvart.Projekt.domain.Group;
import MojKvart.Projekt.service.*;

@RestController
@RequestMapping("/groups")
public class GroupController {

	@Autowired
	private GroupService groupService;
	
	@Autowired
	private StudentService studentService;
	
	@GetMapping("")
	public List<Group> listGroups() {
		return groupService.listAll();
	}
	
	@PostMapping("")
	@Secured("ROLE_LEAD")
	public Group createGroup(@RequestBody CreateGroupDTO group,
			@AuthenticationPrincipal User u) {
		return groupService.createGroup(group.getName(), u.getUsername());
	}
	
}
