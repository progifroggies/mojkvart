package MojKvart.Projekt.rest;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import MojKvart.Projekt.domain.Student;
import MojKvart.Projekt.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@GetMapping("")
	public List<Student> listStudents() {
		return studentService.listAll();
	}
	
	@PostMapping("")
	@Secured("ROLE_ADMIN")
	public Student createStudent(@RequestBody Student student) {
		return studentService.createStudent(student);
	}
	
}
