package MojKvart.Projekt.rest;

public class CreateGroupDTO {

	private String name;
	private String leadJmbag;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLeadJmbag() {
		return leadJmbag;
	}
	public void setLeadJmbag(String leadJmbag) {
		this.leadJmbag = leadJmbag;
	}
	
	@Override
	public String toString() {
		return "CreateGroupDTO [name=" + name + ", leadJmbag=" + leadJmbag + "]";
	}
	
}
