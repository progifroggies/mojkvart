package MojKvart.Projekt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import MojKvart.Projekt.domain.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

	int countByJmbag(String jmbag);
	
	Student findByJmbag(String jmbag);
	
}
