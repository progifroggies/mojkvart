package MojKvart.Projekt.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Student {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(unique=true)
	@NotNull
	@Size(min=10, max=10)
	private String jmbag;
	
	private String givenName;
	private String familyName;
	
	private boolean isLead;
	
	public Long getId() {
		return id;
	}

	public String getJmbag() {
	    return jmbag;
	}

	public void setJmbag(String jmbag) {
	    this.jmbag = jmbag;
	}

	public String getGivenName() {
	    return givenName;
	}

	public void setGivenName(String givenName) {
	    this.givenName = givenName;
	}

	public String getFamilyName() {
	    return familyName;
	}

	public void setFamilyName(String familyName) {
	    this.familyName = familyName;
	}

	public boolean isLead() {
		return isLead;
	}

	public void setLead(boolean isLead) {
		this.isLead = isLead;
	}

	@Override
	public String toString() {
		return "Student #" + id + " " + givenName + " " + familyName;
//	            (isLead? " LEAD" : "") + ", jmbag=" + jmbag;
	}

	
}
