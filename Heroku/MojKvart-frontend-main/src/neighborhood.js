var app = angular.module("myApp", ['ngRoute']);
app.controller("myCtrl", function($scope, $http, $window, $route, $routeParams, $location) {

    $scope.cetvrti = []
    $scope.ulice = []

    $scope.role = 0
    
    $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            console.log(data)
            $scope.role = data.data.role;
            console.log($scope.role)
            if ($scope.role != 3) {
                $window.location.href = "Forum.html";
            }
        });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }

    $http.get(
        "https://progifroggies-moj-kvart-be.herokuapp.com/neighborhoods"
    ).then(function(data) {
        //console.log(JSON.stringify(data.data))
        $scope.cetvrti = data.data
        for (const cet in $scope.cetvrti) {
            $scope.showlist.push(0)
            $scope.addlist.push(0)
            $scope.confirmlist.push(0)
            $scope.showExistStreetError.push(0)
        }

        //dohvati ulice
        for (const cetvrt in $scope.cetvrti) {
            $http.get(
                "https://progifroggies-moj-kvart-be.herokuapp.com/neighborhoods/" + $scope.cetvrti[cetvrt].idCetvrti.toString()
            ).then(function(data) {
                var tul = data.data;
                for (const ttul in tul) {
                    $scope.ulice.push(tul[ttul])
                }
                //console.log(JSON.stringify(data.data))
            });
        };
        //console.log(JSON.stringify($scope.ulice))
    });

    $scope.showlist = []
    $scope.addlist = []
    $scope.addcet = 0
    $scope.confirmlist = []
    $scope.showExistStreetError = []
    $scope.showExistError = 0

    //console.log(JSON.stringify($scope.cetvrti))
    //console.log(JSON.stringify($scope.ulice))

    $scope.showButton = function(ind) {
        $scope.showlist[ind] = !($scope.showlist[ind])
        updatedark()
    }

    $scope.showAddButton = function(ind) {
        $scope.addlist[ind] = !($scope.addlist[ind])
        updatedark()
    }

    $scope.showConfirmButton = function(ind) {
        $scope.confirmlist[ind] = !($scope.confirmlist[ind])
        updatedark()
    }


    $scope.showAddCetvrt = function(ind) {
        $scope.addcet = !($scope.addcet)
    }

    $scope.addStreet = function(ind, name) {
        console.log(ind.toString() + " " + name)
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/neighborhoods/" + $scope.cetvrti[ind].idCetvrti.toString() + "/add", {
                'imeUlice': name
            }
        ).then(function(data) {
            if (data.data.imeUlice == null) {
                $scope.showExistStreetError[ind] = 1;
            } else {
                window.location.reload()
            }
        });
    }

    $scope.deleteStreet = function(id) {
        console.log(id)
    }

    $scope.novaCetvrt = function(name) {
        console.log(name)
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/neighborhoods/add", {
                'imeCetvrti': name
            }
        ).then(function(data) {
            if (data.data.imeCetvrti == null) {
                $scope.showExistError = 1;
            } else {
                window.location.reload()
            }
        });
    }

    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });
    }

    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    updatedark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains("darkmode") != checkdark()) elements[i].classList.toggle("darkmode");
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }

});

app.config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/:idCetvrti', {
          templateUrl: "neighborhoodEdit.html"
        }).when('/', {
          templateUrl: "neighborhoodList.html"
        })
    }
  ]);