var app = angular.module("myApp", ['ngRoute']);
app.controller("myCtrl", function($scope, $http, $window, $routeParams, $route) {

    //$scope.teme = JSON.parse(localStorage.getItem('teme'))
    //$scope.users = JSON.parse(localStorage.getItem('users'))

    $scope.teme = []
    $scope.users = []
    $scope.listempty = 1
    $scope.param = -1
    $scope.ovaTema = ""
    $scope.odgovori = []
    $scope.mojNaslov = ""
    $scope.formbtn = true;
    $scope.formninput = false;
    $scope.Input1 = null;
    $scope.Input2 = null;
    $scope.KomInput = null;
    $scope.deleteCommV = false;
    $scope.editCommV = false;

    var vm = this;
    vm.deleteTopTemaChoice = false;
    vm.deleteTemaChoice = []
    vm.deleteCommChoice = []
    vm.reportCommChoice = []
    vm.editCommChoice = []

    //console.log(localStorage.getItem('userId'))

    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });
    }


    $scope.role = 0

    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
            "userId": localStorage.getItem("userId")
        }
    ).then(function(data) {
        //console.log(data)
        $scope.role = data.data.role;
        //console.log($scope.role)
        if (data.data.userId == null) {
            $window.location.href = "Login.html";
        }
        if ($scope.role == 3) {
            $window.location.href = "admin.html";
        } else if ($scope.role == -1) {
            $window.location.href = "Login.html";
        }
        if (data.data.blocked) {
            $window.location.href = "Login.html";
        }
    });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }

    $http.post(
        "https://progifroggies-moj-kvart-be.herokuapp.com/forum", {
            'userId': localStorage.getItem("userId")
        }
    ).then(function(data) {
        //console.log(JSON.stringify(data.data))
        $scope.teme = data.data
        for (const tema in $scope.teme) {
            vm.deleteTemaChoice.push(false)
            $scope.listempty = 0
        }
        $http.get(
            "https://progifroggies-moj-kvart-be.herokuapp.com/users"
        ).then(function(data) {
            $scope.users = data.data;
            $scope.param = $routeParams.idTeme;
            //console.log($scope.param);
            $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/forum/" + $scope.param.toString(), {
                    'userId': localStorage.getItem("userId")
                }
            ).then(function(data) {
                //console.log(JSON.stringify(data.data))
                $scope.odgovori = data.data.odgovori;
                $scope.prviod = $scope.odgovori[0];
                $scope.odgovori.shift();
                $scope.ovaTema = data.data.tema;
                $scope.mojNaslov = $scope.ovaTema.naslov
                $scope.ownThread = false;
                if ($scope.prviod.userId == localStorage.getItem('userId') || $scope.role == 1) $scope.ownThread = true;
                for (let i = 0; i < $scope.odgovori; i++) {
                    vm.deleteCommChoice[i] = false;
                    vm.reportCommChoice[i] = false;
                    vm.editCommChoice[i] = false;
                }
                //console.log($scope.odgovori)
                //console.log($scope.prviod)
                //console.log($scope.ovaTema)
            });
            //console.log(JSON.stringify(data.data))
        });
        //console.log(JSON.stringify($scope.ulice))
    });

    $scope.getCurrThreadText = function() {
        return $scope.prviod.sadrzaj;
    }

    $scope.getCurrThreadName = function() {
        return $scope.mojNaslov;
    }


    $scope.getThreadName = function(ind) {
        return $scope.teme[ind].naslov;
    }

    $scope.getUserObj = function(userid) {
        for (const user in $scope.users) {
            if ($scope.users[user].userId == userid) return $scope.users[user];
        }
        return null;
    }



    $scope.getNumberComm = function(ind) {
        return $scope.teme[ind].brojOdgovora;

    }
    $scope.getCommentDesc = function(ind) {
        var myuser = $scope.getUserObj($scope.odgovori[ind].userId);
        $scope.editCommV = false;
        if ($scope.odgovori[ind].userId == localStorage.getItem("userId")) {
            $scope.deleteCommV = true;
            $scope.reportUser = false;
            $scope.editCommV = true;
        } else {
            //$scope.deleteComm = false;
            $scope.reportUser = true;
            if ($scope.odgovori[ind].reported == true) $scope.reportUser = false;
        }
        if ($scope.role == 1) {
            $scope.deleteCommV = true;
            $scope.reportUser = true;
        }
        return "Objavio/la " + myuser.name + " " + myuser.surname;
    }
    $scope.getCommentText = function(ind) {
        var myuser = $scope.getUserObj($scope.odgovori[ind].userId);
        if (myuser.userId == localStorage.getItem("userId") || $scope.role == 1) {
            $scope.deleteCommV = true;
        } else {
            $scope.deleteCommV = false;
        }
        return $scope.odgovori[ind].sadrzaj;
    }

    $scope.getThreadDesc = function(ind) {
        var myuser = $scope.getUserObj($scope.teme[ind].userId);
        if (myuser.userId == localStorage.getItem("userId") || $scope.role == 1) {
            vm.deleteTema = true;
        } else {
            vm.deleteTema = false;
        }
        return "Objavio/la " + myuser.name + " " + myuser.surname;
    }

    $scope.getCurrThreadDesc = function() {
        var myuser = $scope.getUserObj($scope.ovaTema.userId);
        return "Objavio/la " + myuser.name + " " + myuser.surname;
    }

    vm.joinThread = function(ind) {
        //console.log("hi");
        setTimeout(() => { window.location.reload(); }, 50);
        window.location.href = "Forum.html#!/" + $scope.teme[ind].idTeme.toString();
    }

    $scope.goToThread = function(ind) {
        return "Forum.html#!/" + $scope.teme[ind].idTeme.toString();
    }

    vm.deleteTemaShow = function(ind) {
        vm.deleteTemaChoice[ind] = true;
    }

    vm.deleteTemaNo = function(ind) {
        vm.deleteTemaChoice[ind] = false;
    }

    vm.deleteTemaF = function(ind) {
        //console.log(ind)
        if ($scope.role == 0 || $scope.role == 2) $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/deleteOwnPost", {
                'userId': localStorage.getItem("userId"),
                'idTeme': $scope.teme[ind].idTeme
            }
        ).then(function(data) { window.location.reload(); });
        else $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/deletePost", {
                'userId': localStorage.getItem("userId"),
                'idTeme': $scope.teme[ind].idTeme
            }
        ).then(function(data) { window.location.reload(); });
    }

    //$scope.addThread = function() {
    //    return "forum.html#!/" + "add";
    // }

    $scope.openForm = function() {
        if ($scope.forminput == true) {
            $scope.forminput = false;
            $scope.formbtn = true;
            $scope.formbtn2 = false;
        } else {
            $scope.forminput = true;
            $scope.formbtn2 = true;
            $scope.formbtn = false;
        }
    };

    var vm = this;

    $scope.showTemaExist = false
    $scope.temaExistId = 0

    vm.addNewPost = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/add", {
                'userId': localStorage.getItem("userId"),
                'naslov': vm.Input1,
                'sadrzaj': vm.Input2,
                'idIzvjesca': null

            }
        ).then(function(data) {
            if (data.data.datumTeme == null) {
                $scope.showTemaExist = true;
                $scope.temaExistId = data.data.idTeme;
                //console.log(data.data.idTeme);
            } else {
                $scope.formbtn = true;
                $scope.forminput = false;
                window.location.reload();
            }
        });

        /*$scope.formbtn = true;
        $scope.forminput = false;
        window.location.reload();*/


    }

    vm.temaExistsOdi = function() {
        window.location.href = "Forum.html#!/" + $scope.temaExistId.toString();
    }


    /** 
                $scope.addNewPost = function() {
                    //add post to database and post it on the page
        
                    $http.post(
                        "https://progifroggies-moj-kvart-be.herokuapp.com/forum/add", {
                            'userId': localStorage.getItem("userId"),
                            'naslov': $scope.Input1,
                            'komentar': $scope.Input2,
                            'idIzvjesca': null

                        }
                    ).then(function(data) {
                        console.log($scope.Input1);
                        console.log($scope.Input2);
                       if(data.data.datumTeme == null){
                           //Naslov teme postoji
                           //dodat doda
                       }
                    }
                    );

                    $scope.formbtn = true;
                    $scope.forminput = false;
                    //window.location.reload();
                }*/


    vm.editCommShow = function(ind) {
        vm.editCommChoice[ind] = true;
    }

    vm.editCommNo = function(ind) {
        vm.editCommChoice[ind] = false;
    }

    vm.editComm = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/changeAnswer", {
                "idOdgovora": $scope.odgovori[ind].idOdgovora,
                "userId": localStorage.getItem("userId"),
                "sadrzaj": vm.Input2
            }
        ).then(function(data) {
            //console.log(JSON.stringify(data.data))
            window.location.reload();
        });
    }

    vm.deleteTopTemaShow = function() {
        vm.deleteTopTemaChoice = true;
    }

    vm.deleteTopTemaNo = function() {
        vm.deleteTopTemaChoice = false;
    }

    vm.deleteCommShow = function(ind) {
        vm.deleteCommChoice[ind] = true;
    }

    vm.deleteCommNo = function(ind) {
        vm.deleteCommChoice[ind] = false;
    }

    vm.deleteComm = function(ind) {
        if ($scope.role == 1) {
            $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/forum/deleteAnswer", {
                    "idOdgovora": $scope.odgovori[ind].idOdgovora
                }
            ).then(function(data) {
                //console.log(JSON.stringify(data.data))
                window.location.reload();
            });

        } else {
            $http.post(
                "https://progifroggies-moj-kvart-be.herokuapp.com/deleteOwnMessage", {
                    "idOdgovora": $scope.odgovori[ind].idOdgovora
                }
            ).then(function(data) {
                //console.log(JSON.stringify(data.data))
                window.location.reload();
            });

        }
    }

    vm.reportCommShow = function(ind) {
        vm.reportCommChoice[ind] = true;
    }

    vm.reportCommNo = function(ind) {
        vm.reportCommChoice[ind] = false;
    }

    vm.deleteThread = function() {
        if ($scope.role == 1) $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/deletePost", {
                'userId': localStorage.getItem("userId"),
                'idTeme': $scope.prviod.idTeme
            }
        ).then(function(data) { window.location.href = "Forum.html" });
        else $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/deleteOwnPost", {
                'userId': localStorage.getItem("userId"),
                'idTeme': $scope.prviod.idTeme
            }
        ).then(function(data) { window.location.href = "Forum.html" });
    }

    vm.reportUser = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/report", {
                "idOdgovora": $scope.odgovori[ind].idOdgovora,
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            //console.log(JSON.stringify(data.data))
            window.location.reload();
        });


    }

    vm.addNewComm = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/forum/" + $scope.param.toString() + "/add", { // $scope.teme[ind].idTeme.toString(), {
                'userId': localStorage.getItem("userId"),
                'sadrzaj': vm.KomInput
            }
        ).then(function(data) {

            //console.log(JSON.stringify(data.data))
            //console.log(vm.KomInput);
            window.location.reload();
        });


    }


    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    updatedark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains("darkmode") != checkdark()) elements[i].classList.toggle("darkmode");
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }

    let mybutton = document.getElementById("btn-back-to-top");
    window.onscroll = function() {
        scrollFunction();
    };

    //function AddNewPost() {

    //add post to datapase and post it on the page
    //  newform.style.display = "none";
    //}

    function scrollFunction() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {

            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    mybutton.addEventListener("click", backToTop);
    //addpostbutton.addEventListener("click", AddNewPost);

    function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

});

app.config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.
        when('/:idTeme', {
                templateUrl: "ForumTopic.html"
            })
            .when('/', {
                templateUrl: "ForumList.html"
            })

    }
]);