var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {
    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });


    }

    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }


    if (localStorage.getItem("role") != null) {
        if (localStorage.getItem("role") == 0) {
            $scope.stanovnik = true;
            $scope.moderator = false;
            $scope.vjecnik = false;
            $scope.administrator = false;
        }
        if (localStorage.getItem("role") == 1) {
            $scope.stanovnik = false;
            $scope.moderator = true;
            $scope.vjecnik = false;
            $scope.administrator = false;
        }
        if (localStorage.getItem("role") == 2) {
            $scope.stanovnik = false;
            $scope.moderator = false;
            $scope.vjecnik = true;
            $scope.administrator = false;
        }
        if (localStorage.getItem("role") == 3) {
            $scope.stanovnik = false;
            $scope.moderator = false;
            $scope.vjecnik = false;
            $scope.administrator = true;
            window.location.href = "admin.html"
        }

    } else {
        window.location.href = "Login.html"
    }

    let mybutton = document.getElementById("btn-back-to-top");
let buttonplus = document.getElementById("btn-open-form");
let newform = document.getElementById("newform");
let addpostbutton = document.getElementById("AddPost");
window.onscroll = function() {
    scrollFunction();
};

function openform() {
    newform.style.display = "block";
    buttonplus.style.display = "none";

};

function AddNewPost() {

    //add post to datapase and post it on the page
    newform.style.display = "none";
    buttonplus.style.display = "block";
}

function scrollFunction() {
    if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
    ) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}
mybutton.addEventListener("click", backToTop);
buttonplus.addEventListener("click", openform);
addpostbutton.addEventListener("click", AddNewPost);

function backToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}



});