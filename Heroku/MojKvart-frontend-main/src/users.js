var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http, $window) {

    $scope.promijeniShow = true;

    //$scope.zahtjevi = JSON.parse(localStorage.getItem('zahtjevi'))
    //$scope.users = JSON.parse(localStorage.getItem('users'))
    $scope.uloge = ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator']
    $scope.showlist = []
    $scope.notiflist = []
    $scope.banlist = []

    $scope.role = 0

    var vm = this;
    vm.changeRoleChoice = [];
    vm.roleToBe = [];
    
    $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/profil", {
                "userId": localStorage.getItem("userId")
            }
        ).then(function(data) {
            //console.log(data)
            $scope.role = data.data.role;
            //console.log($scope.role)
            if ($scope.role != 3) {
                $window.location.href = "Forum.html";
            }
        });

    $scope.getUserRoleText = function() {
        return ['Stanovnik', 'Moderator', 'Vijećnik', 'Administrator'][$scope.role];
    }

    $http.get(
        "https://progifroggies-moj-kvart-be.herokuapp.com/users"
    ).then(function(data) {
        //console.log(JSON.stringify(data.data))
        $scope.users = data.data
        $http.get(
            "https://progifroggies-moj-kvart-be.herokuapp.com/checkRequests"
        ).then(function(data) {
            $scope.zahtjevi = data.data
            for (const user in $scope.users) {
                vm.changeRoleChoice.push(false)
                vm.roleToBe.push(0)
                $scope.showlist.push(0)
                $scope.banlist.push($scope.users[user].blocked)
                for (const zahtjev in $scope.zahtjevi) {
                    if ($scope.zahtjevi[zahtjev].userId == $scope.users[user].userId) $scope.notiflist[user] = 1
                }
            }
            //console.log(JSON.stringify(data.data))
        });
        //console.log(JSON.stringify($scope.ulice))
    });
    /*
    for (const user in $scope.users) {
        $scope.showlist.push(0)
        for (const zahtjev in $scope.zahtjevi) {
            if ($scope.zahtjevi[zahtjev].userId == $scope.users[user].userId) $scope.notiflist[user] = 1
        }
    }*/

    $scope.logout = function() {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/logoutUser", {}
        ).then(function() {
            localStorage.removeItem("role");
            localStorage.removeItem("userId");
            $window.location.href = "Login.html";
        });
    }

    $scope.setRoleToBe = function(ind, val) {
        vm.roleToBe[ind] = val;
        vm.changeRoleChoice[ind] = true;
        //console.log(vm.roleToBe)
        //console.log(vm.changeRoleChoice)
    }

    $scope.setRoleNo = function(ind) {
        vm.changeRoleChoice[ind] = false;
        $scope.showlist[ind] = 0;
    }

    $scope.setRole = function(ind) {
        //console.log($scope.users[ind].userId)
        var val = vm.roleToBe[ind];
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/users/changeRole", {
                // streetId je tu userId od administratora koji radi promjenu!!
                'streetId': localStorage.getItem("userId"),
                'userId': $scope.users[ind].userId,
                'role': val
            }
        ).then(function() {
            $window.location.reload()
        });
    }

    $scope.setShow = function(ind, val) {
        $scope.showlist[ind] = val
    }

    $scope.blockUser = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/users/ban", {
                'userId': $scope.users[ind].userId,
            }
        ).then(function() {
            $window.location.reload()
        });
    }

    $scope.unblockUser = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/users/unban", {
                'userId': $scope.users[ind].userId,
            }
        ).then(function() {
            $window.location.reload()
        });
    }

    $scope.deleteUser1 = function(ind) {
        $http.post(
            "https://progifroggies-moj-kvart-be.herokuapp.com/users/delete", {
                'userId': $scope.users[ind].userId,
            }
        ).then(function() {
            $window.location.reload()
        });
    }

    $scope.goToRequests = function(ind) {
        window.location.href = "checkRequests.html"
    }

    $scope.getZahtjevString = function(ind) {
        //console.log($scope.zahtjevi[ind])
        var wantrole = 0
        for (const zahtjev in $scope.zahtjevi) {
            if ($scope.zahtjevi[zahtjev].userId == $scope.users[ind].userId) wantrole = $scope.zahtjevi[zahtjev].vrstaZatrazenogRolea;
        }
        return "Ovaj korisnik ima trenutno aktivan zahtjev za promjenu uloge u " + $scope.uloge[wantrole] + "a"
    }
    $scope.deleteUser = false;
    $scope.banUser = false;
    $scope.getUserString = function(ind) {
        //console.log($scope.zahtjevi[ind])

        if ($scope.users[ind].reports > 1) {
            $scope.deleteUser = true;
        } else {
            $scope.deleteUser = false;
        } 
        if ($scope.users[ind].reports >= 1) {
            $scope.banUser = true;
        }
        else{
            $scope.banUser = false;
        }
        if($scope.users[ind].role == 3){
            $scope.banUser = false;
            $scope.deleteUser = false;
            $scope.promijeniShow = false;
        }
        else{
             $scope.promijeniShow = true;
        }

        var ts = $scope.users[ind].name + " " + $scope.users[ind].surname + " (" + $scope.users[ind].email + "), [" + $scope.uloge[$scope.users[ind].role] + "], Broj Prijava: " + $scope.users[ind].reports

        if ($scope.users[ind].blocked) ts += " [KORISNIK BLOKIRAN]"
        return ts
    }

    // DarkMode Button Script:

    checkdark = function() {
        var wastrue = 0;
        let decodedCookie = decodeURIComponent(document.cookie);
        //alert(decodedCookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf("darkmode") == 0) {
                //alert("found");
                //alert(c);
                if (c == 'darkmode=TRUE') {
                    wastrue = 1;
                }
            }
        }
        return wastrue;
    }
    swapdark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            elements[i].classList.toggle("darkmode")
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    updatedark = function() {
        //alert("test")
        elements = document.querySelectorAll("*")
        for (i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains("darkmode") != checkdark()) elements[i].classList.toggle("darkmode");
        }
        element = document.getElementById("btn-darkmode")
        if (element.classList.contains("darkmode")) {
            element.innerHTML = "<b>DARK MODE</b>";
        } else {
            element.innerHTML = "<b>LIGHT MODE</b>";
        }
    }
    $scope.darkmodefunc = function() {
        swapdark();
        var wastrue = checkdark();
        var cookie;
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        if (wastrue) {
            cookie = "darkmode" + " = " + encodeURIComponent("FALSE") + "; " + expires + "; path=/";
        } else {
            cookie = "darkmode" + " = " + encodeURIComponent("TRUE") + "; " + expires + "; path=/";
        }
        //alert(cookie);
        document.cookie = cookie;
    }

    if (checkdark()) {
        swapdark();
    }


});