Link na MojKvart stranicu: https://progifroggies-moj-kvart.herokuapp.com/

Podatci za prijavu:

admin.adminic@fer.hr	passAdmin	Administrator

admin.adminko@fer.hr	passAdmin2	Administrator

katarina.brnic@fer.hr	pass1		Običan stanovnik	Srednjaci

stjepan.grdi@fer.hr		pass2		Moderator			Srednjaci

edi.kolac@fer.hr		pass3		Vijećnik			Srednjaci

tea.teic@fer.hr			pass		Običan stanovnik	Vrbani

sara.saric@fer.hr		passs		Moderator			Vrbani

una.unic@fer.hr			pass		Vijećnik			Vrbani

ivo.ivic@fer.hr			pass		Običan stanovnik	Maksimir

mod3@fer.hr				pass		Moderator			Maksimir

ante.antic@fer.hr		pass		Vijećnik			Maksimir

izbrisan@fer.hr														Trajno izbrisan korisnik

blokiran@fer.hr			pass		Običan korisnik		Srednjaci	Privremeno blokiran korisnik

Napomena: Heroku ponekad, iz sigurnosnih razloga, mijenja ime i šifru za pristup svojoj bazi podataka.
Ako se takvo nešto slučajno desi prije nego što Vi provjeravate projekt, backend se neće moći spojiti na bazu jer su zapisani podatci krivi.
U tom bih Vas slučaju zamolila da promijenimo jednu datoteku, riječ je jedino o application.properties u src/main/resources.